# Skill Issue Rules

0. Members must have at least one discernible _skill issue_. A skill issue is defined as a lack of experience or knowledge regarding a particular topic, no matter how specific or broad. Members should expect to be playfully called out on their skill issue(s).

0. Heed the terms of service and all other applicable policies governing acceptable use of the Matrix homeserver(s), client(s), and protocol(s) used in conjunction with Skill Issue, as well as any ancillary platforms and software that may be used in connection with Skill Issue or any of its projects and properties.

0. Bigotry, hate speech, and intolerance for others' personal and subjective beliefs are strictly prohibited and grounds for an immediate (zero&#8209;warning) permanent ban. This includes racism, sexism, and other forms of prejudice discrimination. Jokes of this nature or to such an end are equally impermissible.

   - Probing questions and/or a single warning _may_ be issued if there is judged to be reasonable room for alternate (non&#8209;incriminating) interpretation.

0. Exhibit and exude respect toward all current and former Skill&nbsp;Issue members. Personal attacks are strictly prohibited.

   - The content and behavior presented in members' messages and other interactions constitute an acceptable basis for constructive criticism, as long as it does not rise to the level of outright toxicity.

   - Harmless banter in moderation is acceptable, as long as it does not come at the sacrifice of respect. _This necessarily means a higher standard of professionalism is expected toward newer or less active members._

0. Linking, uploading, or otherwise directly facilitating access to pirated or NSFW content is strictly prohibited.

0. General undesirable or nuisance behavior, even if not explicitly restricted, is frowned-upon and may be grounds for moderation action. This includes but is not limited to impersonation, spamming, advertising, excessive use of languages other than modern English, and a blatant disregard for facts, logic, and/or alternative viewpoints. Put simply, _don't be annoying_.

0. The moderators and admins (administrators) have the final verdict regarding enforcement of rules. In general, for non-egregious violations on separate occasions, two warnings should be expected followed by a mute and/or ban&nbsp;&mdash; but **this is only a baseline and individual circumstances may vary**.

   - If you feel a moderator or admin action was executed unfairly or in bad faith, please discuss the matter in a direct message with an uninvolved admin (noting that they are under no obligation to listen). Do not attempt to evade a ban or otherwise circumvent a moderation action or directive.

0. Permanent bans extend to all alt (alternate) accounts and across all rooms contained in the Skill Issue space as well as the space itself and any ancillary platforms and properties maintained as part of Skill Issue.

   - The breadth of lesser moderation actions shall be decided on a case&#8209;by&#8209;case basis and reflected by explicit permission changes.

0. All rules and directives are to be interpreted as a [reasonable person](https://www.law.cornell.edu/wex/reasonable_person) would interpret them.

These rules are subject to change at any time. A reasonable effort will be expended to provide notice of any major changes, but receipt of notice is not guaranteed. _Subscribing to the [Skill Issue Rules](https://gitlab.com/skill-issue/rules) repository on GitLab is recommended as a more robust mechanism to receive notice of changes&nbsp;&mdash; via 'Watching' the repository (requires a GitLab account) or the [Atom (RSS) feed](https://gitlab.com/skill-issue/rules.atom)._

## Guidelines and Best Practices

The following are not enforced as rules but are nevertheless strongly recommended to improve the overall experience for everyone:

- Messages should stand on their own as complete thoughts. Conversely, a single complete thought should not span three or four, or even more, messages.

- Hyperlinks should be [labeled with appropriate link text](https://developer.mozilla.org/en-US/docs/Learn/HTML/Introduction_to_HTML/Creating_hyperlinks#link_best_practices "MDN&nbsp;&mdash; Creating hyperlinks&nbsp;&mdash; Link best practices") and/or accompanied by body text describing the nature of the linked resource, especially when lacking the context of an existing conversation. Self-documenting hyperlinks (e.g. with keywords embedded in the URL) are _sometimes_ sufficient to fulfill this purpose.